const config = require('../config');
const Bigtable = require('@google-cloud/bigtable');

class DB {
    static getInstance () {
        const bigtable = new Bigtable({
            projectId: config.db.projectId,
        });
        return bigtable.instance(config.db.instanceId);
    }
}

module.exports = DB;
