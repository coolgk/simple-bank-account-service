'use strict';

module.exports = {
    db: {
        instanceId: process.env.GOOGLE_BIGTABLE_INSTANCE_ID,
        projectId: process.env.GOOGLE_BIGTABLE_PROJECT_ID
    },
    corsDomains: process.env.CORS_DOMAINS.split(',')
};
