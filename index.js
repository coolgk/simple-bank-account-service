const express = require('express');
const cors = require('cors');
const helmet = require('helmet');

const config = require('./config');

const DB = require('./core/DB');

const app = express();

app.use(helmet());
app.set('trust proxy', 1); // trust first proxy

app.use(
    cors({
        origin: config.corsDomains,
        credentials: true
    })
);

app.get('/account/:accountId', async (request, response) => {
    const instance = DB.getInstance();
    const accountTable = instance.table('account');
    const [ account ] = await accountTable.row(request.params.accountId).get({
        filter: [
            {
                column: {
                    cellLimit: 1, // Only retrieve the most recent version of the cell.
                },
            }
        ]
    }).catch((error) => {
        console.error(error);
        response.status(404).send('Not Found');
    });

    response.json({
        id: account.id,
        balance: account.data.summary.balance[0].value,
        name: account.data.summary.name[0].value
    });
});

app.listen(8000);

// ============= GLOBAL ERROR HANDLER (THIS DOES NOT CATCH UNHANDLED REJECTIONS)
// next is an unused variable but required for this callback
app.use((error, request, response, next) => { // eslint-disable-line no-unused-vars
    // attach a custom error logger
    console.error(error);
    response.status(500).send('Internal Server Error');
});

process.on('unhandledRejection', (error) => {
    // attach a custom error logger
    console.error(error);
    console.log('global error logger'); // tslint:disable-line
});
